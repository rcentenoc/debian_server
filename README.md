# SERVIDOR DEBIAN CON SAMBA



## Introducción
Un servidor Samba nos permite crear un Servidor de Archivos y Recursos compartidos, y administrarlos otorgando los permisos deseados a los usuarios y grupos que creemos, de esta forma podremos compartir archivos y directorios desde equipos Linux a equipos Windows (y también con equipos Linux, claro).

Existen algunas herramientas gráficas para ese fin, así como la herramienta para administrar Samba vía web «SWAT». Sin embargo, recomendamos a los principiantes que se inicien en éste maravilloso mundo de forma manual. No es tan difícil ni diabólico como muchos piensan. Y en el proceso se aprende mucho sobre redes SMB/CIFS y sobre Permisos y Derechos en sistemas de archivos Linux.

Tecnología usada:

#### VMWARE WORKSTATION

VMware Workstation Pro es un hipervisor alojado que se ejecuta en versiones x64 de los sistemas operativos Windows y Linux  (estaba disponible una versión x86 de versiones anteriores); permite a los usuarios configurar máquinas virtuales (VM) en un solo máquina y utilícelos simultáneamente junto con la máquina host. 

Cada máquina virtual puede ejecutar su propio sistema operativo, incluidas las versiones de Microsoft Windows, Linux, BSD y MS-DOS. Las máquinas virtuales Linux listas para usar y configuradas para diferentes propósitos están disponibles en varias fuentes.

#### SAMBA
Y dado que Samba utiliza el protocolo SMB, podemos acceder como clientes SMB desde diversos sistemas operativos y por diversos medios. Toda la configuración restante del Servidor Samba, necesaria para su funcionamiento.

### COMANDOS LINUX usados

```html
sudo apt install samba
sudo apt install smbclient
sudo apt update && sudo apt upgrade -y
sudo apt install -y samba
sudo ifconfig
ip address
sudo ufw allow samba
sudo nano /etc/samba/smb.conf

```
### COMANDOS PARA CREAR USUARIOS EN SAMBA
```html
useradd sambauser
passwd sambauser
smbpasswd -a sambauser
pdbedit -L
useradd -g SAMBAGROUP sambauser
usermod -g SAMBAGROUP sambauser
smbpasswd -a sambauser
chmod ### /srv/samba/SAMBAGROUP
chown -hR root:sambagroup srv/samba/SAMBAGROUP
```

### COMANDOS WINDOWS usados

```html
ipconfig
ping xxx.xxx.xxx.xxx
```
### Enlaces de ayuda

> https://eltallerdelbit.com/comandos-basicos-debian/
> https://chachocool.com/como-instalar-samba-en-debian-9-stretch/
> https://eltallerdelbit.com/gestion-usuarios-grupos-permisos-samba/
> https://eltallerdelbit.com/clientes-smb-samba/
> https://eltallerdelbit.com/servidor-samba/
> https://www.linkedin.com/in/peiruza/
> https://www.samba.org/samba/docs/
> http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m4/servidor_samba.html
> https://www.lpi.org/our-certifications/lpic-2-overview
> http://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-rg-es-4/s1-samba-daemons.html

### End

ENLACE DEL VIDEO

https://www.canva.com/design/DAEQGfMSOdw/mK6KD5_yh5uNvyq-_Ywm6g/watch?utm_content=DAEQGfMSOdw&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink