# Primera parte
### INSTALACIÓN DE  SAMBA SERVER Y CLIENTE SAMBA

En esta primera parte instalamos el servidor Samba, con sus dos demonios: smbd (comparte recursos por SMB) y nmbd (compatibilidad con NETBIOS de Windows), esto nos dara paso a configurar el Servidor Samba y los recursos compartidos (configurando el archivo smb.conf).

### Comandos usados
```
apt install samba #Para instalar el servidor
apt install smbclient #Para instalar el cliente
```
### Enlaces de ayuda

>https://eltallerdelbit.com/comandos-basicos-debian/
>https://chachocool.com/como-instalar-samba-en-debian-9-stretch/
>https://eltallerdelbit.com/servidor-samba/


### End