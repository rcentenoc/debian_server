# Segunda parte
### CONFIGURAR SAMBA

El fichero de configuración principal de Samba es
```
 /etc/samba/smb.conf
```
Dentro del archivo smb.conf hay algunos aspectos básicos que hemos de configurar correctamente para que el servidor Samba funcione, como por ejemplo:

- La interfaz del equipo Linux que escucha y comparte los recursos a través de la red conectada a dicha interfaz.
- El nombre de Dominio o Grupo de trabajo (WORKGROUP).
- Podemos habilitar el soporte WINS (Servidor de nombres de Microsoft para NETBIOS).
- Podemos configurar el rol del servidor: standalone server, member server o classic primary domain controller.
- Los Recursos Compartidos en un Servidor Samba:
> Hemos de definir el nombre de/los recursos compartidos
Los permisos (si es de solo escritura o solo lectura, si solo pueden leer o escribir ciertos grupos de usuarios …)
El directorio que se comparte (Hemos de concretar el Path del directorio que se compartirá)


### Comandos usados
```
apt install samba #Para instalar el servidor
apt install smbclient #Para instalar el cliente
```
### Enlaces de ayuda
>https://es.wikipedia.org/wiki/Windows_Internet_Naming_Service
>https://eltallerdelbit.com/servidor-samba/


### End