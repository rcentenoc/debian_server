# Tercera parte
### CREACIÓN DE RECURSOS COMPARTIDOS SAMBA

Crearemos varios recursos compartidos dentro del fichero :
```
smb.conf.
```
- Uno de ellos permitirá a 1 usuario y/o 1 grupo de usuarios que puedan escribir, y todo el mundo pueda leer (recurso público). 
El cual creara un recurso público, sin restricciones, para que pueda acceder cualquier usuario, incluso invitados. Aunque esos usuarios solamente tendrán permisos de lectura.

```
[publico]
comment = publico....
path = /srv/samba/publico
browseable = yes
read only  = no
guest ok   = yes
write list = @root
```
- Creamos un recurso compartido en el que solo los usuarios de un grupo tengan permisos de escritura, y los usuarios de otro grupo tengan permisos de lectura. Como decíamos, también crearemos un recurso solamente accesible por los usuarios pertenecientes a cierto grupo de usuarios.
En este caso crearemos el recurso administracion, al que podrán acceder los usuarios de los grupos administracion y jefes, pero solo los usuarios del grupo administracion podrán escribir en el directorio y sus archivos.

```
[administracion]
comment = solo para grupo administracion
path = /srv/samba/administracion
browseable = yes
read only  = yes
guest ok   = no
write list = @administracion
valid users = @administracion, @jefes
```
- Creamos en la máquina los directorios donde se montarán los recursos compartidos Samba. Los recursos compartidos Samba han de ser creados en algún directorio del Servidor, y otorgados también los permisos adecuados a los usuarios.
En este caso crearemos los directorios, por ejemplo en /srv/samba/

Y  ya dentro de dicho Path, crearemos los recursos comentados anteriormente (estos Path son los que habremos de incluir posteriormente en el fichero de configuración smb.conf, cuando declaremos la creación de los recursos compartidos).

Así que creamos:

>Un recurso de acceso libre, de solo lectura (/srv/samba/publico)

>Un recurso al que solo podrán acceder los miembros de 2 grupos de usuarios, y solo los miembros de uno de esos grupos podrán escribir. (/srv/samba/administracion)

- Configuramos las interfaces del equipo con las que trabajará el Servidor Samba. Si bien este apartado hemos de configurarlo en el archivo smb.conf, vamos a verlo dentro de este punto, en el que estamos configurando los recursos compartidos por Samba.

```
#### Networking ####
# The specific set of interfaces / networks to bind to 
# This can be either the interface name or an IP address/netmask; 
# interface names are normally preferred 
;   interfaces = 127.0.0.0/8 eth0 
interfaces = 192.168.0.160/24 eth0
```



### Comandos usados
```
mkdir /srv/samba/publico
mkdir /srv/samba/administracion

```
### Enlaces de ayuda
>https://es.wikipedia.org/wiki/Windows_Internet_Naming_Service
>https://eltallerdelbit.com/servidor-samba/


### End