# Cuarta parte
### GESTIÓN DE USUARIOS, PERMISOS Y GRUPOS SAMBA

Es fundamental la gestión de usuarios, permisos y grupos Samba.

No solo hemos de controlar desde el Servidor Samba los permisos que tendrán los usuarios y grupos a ciertos recursos compartidos, sino que estos permisos guardarán relación con los permisos que tengan dichos usuarios creados también de forma local en el Servidor Samba.

Al igual que los usuarios, los recursos compartidos no solo aplicarán los permisos configurados para dichos recursos en el archivo smb.conf, sino que se tendrán en cuenta los permisos asignados a los directorios de la máquina que conformarán esos recursos compartidos.

- Hacemos un testparm para validar el contenido del fichero de configuración smb.conf

Si no hay errores, se listarán los servicios cargados.

```
testparm smb.conf
#Loaded services file OK.

```
### PRUEBAS SAMBA DESDE CLIENTES

Ahora desde otro equipo situado en la red, accedemos al explorador de archivos y equipos de red. En este caso lo hacemos desde Debian, pero podemos comprobarlo desde Ubuntu, Suse, o cualquier distro Linux que tenga instalado el cliente smb…  igualmente desde clientes Windows (que ya utilizan el protocolo smb para compartir archivos y acceder a recursos compartidos).
- Examinamos la red desde el explorador de archivos y red.
- Accedemos al WORKGROUP o DOMINIO adecuado.
- Accedemos al recurso compartido publico.
- Vemos los ficheros dentro del recurso compartido

Ahora accedemos al recurso administración, en este caso a través del navegador de archivos de Linux, con el usuario correcto.

### Comandos usados
```
systemctl restart smbd.service
/etc/init.d/smbd restart
```
### Enlaces de ayuda
> https://www.linkedin.com/in/peiruza/
> https://www.samba.org/samba/docs/
> http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m4/servidor_samba.html
> https://www.lpi.org/our-certifications/lpic-2-overview
> http://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-rg-es-4/s1-samba-daemons.html
> https://eltallerdelbit.com/servidor-samba/


### End