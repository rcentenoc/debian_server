# INSTALACIÓN DEL VM-WARE workstation
VMware Workstation Pro es un hipervisor alojado que se ejecuta en versiones x64 de los sistemas operativos Windows y Linux  (estaba disponible una versión x86 de versiones anteriores); permite a los usuarios configurar máquinas virtuales (VM) en un solo máquina y utilícelos simultáneamente junto con la máquina host. 

Cada máquina virtual puede ejecutar su propio sistema operativo, incluidas las versiones de Microsoft Windows, Linux, BSD y MS-DOS. Las máquinas virtuales Linux listas para usar y configuradas para diferentes propósitos están disponibles en varias fuentes.

##### OJO: Dejo el instalador del VM ware para quien quiera instalarlo en versión 16-64x

### Motivos por los que se debe usar VM ware

- Se puede crear aplicaciones nativas de nube, modernizando aplicaciones existentes y gestionando una infraestructura para cualquier nube.
- Se puede gestionar y controlar un entorno de forma coherente en las nubes públicas, privadas e híbridas.
- Se puede facilitar el trabajo de los empleados en cualquier lugar, momento y dispositivo sin poner en riesgo la seguridad.
- Se puede conectar y protejer aplicaciones o datos, independientemente de dónde se ejecuten: centro de datos, nube o perímetro.
- Se puede utilizar su infraestructura y puntos de control para proteger aplicaciones y datos, del punto de acceso a la nube.


### Serial KEYS
```
ZF3R0-FHED2-M80TY-8QYGC-NPKYF
YF390-0HF8P-M81RQ-2DXQE-M2UT6
ZF71R-DMX85-08DQY-8YMNC-PPHV8
AZ3E8-DCD8J-0842Z-N6NZE-XPKYF
FC11K-00DE0-0800Z-04Z5E-MC8T6
```

### Enlaces de ayuda

> https://gist.github.com/gopalindians/ec3f3076f185b98353f514b26ed76507#file-vmware-workstation-16-pro-serial-key
> https://isha.sadhguru.org/mahashivratri/sadhguru/articles/shambho-a-gentle-form-of-shiva/
> https://gist.github.com/gopalindians
> https://www.vmware.com/latam.html

### End